# datahike-invoice

This is a simple demonstration repository to explore datalog with
[datahike](https://github.com/replikativ/datahike) to create an invoice in PDF
form with the help of [selmer](https://github.com/yogthos/Selmer/) and pdflatex.

## Usage

Fire up a Clojure repl and walk through the core namespace manually.

## Presentations

Presentations covering this repository can be found in the `doc` folder.

## License

Copyright © 2018-2020 Konrad Kühne, Christian Weilbach

Distributed under the MIT License.
