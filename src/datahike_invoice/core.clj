(ns datahike-invoice.core
  (:require [datahike.api :as d]
            [selmer.parser :refer [render-file]]
            [selmer.filters :refer [add-filter!] :as filters]
            [clojure.java.shell :as sh]
            [com.rpl.specter :as S])
  (:import [java.util Calendar]))

(def uri "datahike:file:///tmp/invoicing")

(def schema [;; customer
             {:db/cardinality :db.cardinality/one
              :db/ident :customer/name
              :db/unique :db.unique/identity
              :db/valueType :db.type/string}
             {:db/ident :customer/department
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :customer/contact
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :customer/street
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :customer/postal
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :customer/city
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :customer/country
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}

             ;; task
             {:db/ident :task/name
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :task/effort
              :db/valueType :db.type/long
              :db/cardinality :db.cardinality/one}
             {:db/ident :task/effort-unit
              :db/valueType :db.type/ref
              :db/cardinality :db.cardinality/one}
             {:db/ident :task/effort-price
              :db/valueType :db.type/long
              :db/cardinality :db.cardinality/one}
             {:db/ident :task/price-unit
              :db/valueType :db.type/ref
              :db/cardinality :db.cardinality/one}
             {:db/ident :hour}
             {:db/ident :day}
             {:db/ident :euro}
             {:db/ident :sek}
             ;; task group
             {:db/ident :task-group/tasks
              :db/valueType   :db.type/ref
              :db/cardinality :db.cardinality/many}
             {:db/ident :task-group/name
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             ;; offer
             {:db/ident  :offer/name
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :offer/number
              :db/valueType :db.type/string
              :db/unique :db.unique/identity
              :db/cardinality :db.cardinality/one}
             {:db/ident :offer/advisor
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :offer/task-groups
              :db/valueType :db.type/ref
              :db/cardinality :db.cardinality/many}])

(comment

  (d/delete-database uri)

  (d/create-database uri :initial-tx schema)

;;
  )

(def conn (d/connect uri))

(d/transact conn [{:customer/department "IT Services"
                   :customer/name       "FunkyHub Startup"
                   :customer/contact    ""
                   :customer/street     "Huvudvägen 1"
                   :customer/postal     "11223"
                   :customer/city       "Stockholm"
                   :customer/country    "Sweden"}
                  {:customer/department ""
                   :customer/name       "Little Shop"
                   :customer/contact    "Herr Schmitt"
                   :customer/street     "Marktplatz 10"
                   :customer/postal     "23455"
                   :customer/city       "Heidelberg"
                   :customer/country    "Germany"}])

;; retrieve clients
(d/q '[:find ?e ?on
       :where
       [?e :customer/name ?on]]
     @conn)

(defonce cal (Calendar/getInstance))

(defn format-num [n]
  (if (< n 10) (str 0 n) n))

(defn create-ref []
  (let [y (.get cal Calendar/YEAR)
        m (inc (.get cal Calendar/MONTH))
        d (.get cal Calendar/DATE)
        date (str y (format-num m) (format-num d))]
    (loop [offset 1]
      (let [ref (str date "-" (format-num offset))]
        (if-not (d/entity @conn [:offer/number ref])
          ref
          (recur (inc offset)))))))


;; fails because customer is not defined


(let [task-group-id (d/tempid -1)
      task-ids (vec (for [i (range 2)] (d/tempid -1)))]
  (d/transact conn [{:db/id             (task-ids 0)
                     :task/name         "Adjustment Login Screen"
                     :task/effort       1
                     :task/effort-unit  :hour
                     :task/effort-price 75
                     :task/price-unit   :euro}
                    {:db/id             (task-ids 1)
                     :task/name         "Extend database schema"
                     :task/effort       4
                     :task/effort-unit  :hour
                     :task/effort-price 75
                     :task/price-unit   :euro}
                    {:db/id             task-group-id
                     :task-group/name  "Fresh Produce Shop"
                     :task-group/tasks  task-ids}
                    {:customer/_offers [:customer/name "Little Shop"]
                     :offer/name        "Adjustments App Q2 2019"
                     :offer/advisor     "Konrad Kühne"
                     :offer/number (create-ref)
                     :offer/task-groups [task-group-id]}]))

;; add missing schema and add again
(d/transact conn [{:db/ident :customer/offers
                   :db/valueType :db.type/ref
                   :db/cardinality :db.cardinality/many}])

(def as-of-date (java.util.Date.))

(d/q '[:find ?or .
       :where
       [?e :offer/number ?or]]
     @conn)

(d/q '[:find [(pull ?tg [*]) ...]
       :where
       [?tg :task-group/name ?tgn]]
     @conn)

;; schema not present
(d/transact conn [{:something "123"}])

;; invalid data type
(d/transact conn [{:task/effort "23"}])

(d/pull @conn '[*] [:customer/name "Little Shop"])

(d/pull @conn '[* {:customer/offers [* {:offer/task-groups [:task-group/name {:task-group/tasks [* {:task/price-unit [:db/ident] :task/effort-unit [:db/ident]}]}]}]}]
        [:customer/name "Little Shop"])


(def i18n {:hour "hour"
           :day "day"})

(defn translate [e]
  [:safe (get i18n e (str "NOT-FOUND: " e))])

(def units {:euro "\\euro{}"
            :sek "SEK"})

(defn unit [e]
  [:safe (get units e (str "NOT-FOUND: " e))])

(defn price-format [e]
  [:safe (String/format java.util.Locale/UK "%.2f" (into-array [(double e)]))])

(defn effort-format [e]
  [:safe (String/format java.util.Locale/UK "%.1f" (into-array [(double e)]))])

(add-filter! :translate translate)

(add-filter! :unit unit)

(add-filter! :pf price-format)

(add-filter! :ef effort-format)

(comment
  (filters/call-filter :ef 2.0)

  (filters/remove-filter! :ef))

(defn transform-task [offer-map]
  (S/transform [:offer/task-groups S/ALL :task-group/tasks S/ALL]
               (fn [{:keys [task/effort task/effort-price] :as task}]
                 (-> task
                     (assoc :task/total-effort (* effort effort-price))
                     (update :task/price-unit :db/ident)
                     (update :task/effort-unit :db/ident)))
               offer-map))

(defn transform-task-group [offer-map]
  (S/transform [:offer/task-groups S/ALL]
               (fn [{:keys [task-group/tasks] :as task-group}]
                 (assoc task-group
                        :task-group/price
                        (reduce + (map :task/total-effort tasks))
                        :task-group/price-unit
                        (:task/price-unit (first tasks))))
               offer-map))

(defn render-pdf [{:keys [offer-id id-prefix is-offer? name-prefix footer output-name]}]
  (let [client (d/q '[:find (pull ?e [*]) .
                      :in $ ?offer-id
                      :where
                      [?e :customer/offers ?offer-id]]
                    @conn
                    offer-id)
        oid (:db/id (d/entity @conn offer-id))
        offer (d/pull @conn '[* {:offer/task-groups
                                 [:task-group/name {:task-group/tasks [* {:task/effort-unit [:db/ident] :task/price-unit [:db/ident]}]}]}]
                      oid)

        offer-map (merge client (assoc offer :offer/id oid))
        offer-map (->> (merge client (assoc offer :offer/id oid))
                       transform-task
                       transform-task-group)
        total-price (reduce + (map :task-group/price (:offer/task-groups offer-map)))
        tax-rate 19
        tax (* (Math/ceil (* tax-rate total-price)) 0.01)
        offer-map (assoc offer-map
                         :offer/name-prefix name-prefix
                         :offer/id-prefix id-prefix
                         :offer/footer footer
                         :offer/total-price total-price
                         :offer/tax tax
                         :offer/price-unit (:task-group/price-unit
                                            (first (:offer/task-groups offer-map)))
                         :offer/brutto (+ tax total-price))
        output-name (str (if is-offer? "offer_" "invoice_") (:offer/number offer-map))
        output-tex (str output-name ".tex")]
    (spit (str "resources/" output-tex) (render-file "template.tex" offer-map))
    (sh/sh "pdflatex" output-tex :dir "resources")
    (sh/sh "rm" (str output-name ".log") :dir "resources")
    (sh/sh "rm" (str output-name ".aux") :dir "resources")
    output-name))

(defn render-offer [offer-id]
  (render-pdf {:is-offer? true
               :output-name (str "offer_" offer-id)
               :name-prefix "Offer for "
               :id-prefix "Your Offer No."
               :footer "Services exceeding the offer are billed separately."
               :offer-id offer-id}))

(defn render-invoice [invoice-id]
  (render-pdf {:name-prefix "Invoice for "
               :id-prefix "Your Invoice No."
               :footer "The invoice is for the work done in this Month. The payment is due in 7 days from invoice date."
               :offer-id invoice-id}))

;; list all customer offers
(d/pull @conn '[{:customer/offers [:offer/number :offer/name]}]
        [:customer/name "Little Shop"])

(defn customer-offers [customer-name]
  (d/pull @conn '[{:customer/offers [:offer/number :offer/name]}]
          [:customer/name customer-name]))

(render-invoice [:offer/number (-> (customer-offers "Little Shop") :customer/offers first :offer/number)] )

;; let's add something for the other client
(let [task-group-id (d/tempid -1)
      task-ids (vec (for [i (range 2)] (d/tempid -1)))]
  (d/transact conn [{:db/id             (task-ids 0)
                     :task/name         "Create Canvas"
                     :task/effort       2
                     :task/effort-unit  :day
                     :task/effort-price 9000
                     :task/price-unit   :sek}
                    {:db/id             (task-ids 1)
                     :task/name         "Create toolchain"
                     :task/effort       4
                     :task/effort-unit  :day
                     :task/effort-price 9000
                     :task/price-unit   :sek}
                    {:db/id             task-group-id
                     :task-group/name  "Web App"
                     :task-group/tasks  task-ids}
                    {:customer/_offers    [:customer/name       "FunkyHub Startup"]
                     :offer/name        "Prototype of Economic Modelling App"
                     :offer/advisor     "Christian Weilbach"
                     :offer/number (create-ref)
                     :offer/task-groups [task-group-id]}]))

(def offer-no (-> (customer-offers "FunkyHub Startup")
                   :customer/offers first :offer/number))

(render-offer [:offer/number offer-no])

;; change offer number
(d/transact conn [{:db/id [:offer/number offer-no]
                   :offer/number "20190205-01"}])

(customer-offers "FunkyHub Startup")

(render-offer [:offer/number offer-no])

(def offer-no (-> (customer-offers "FunkyHub Startup")
                  :customer/offers first :offer/number))

(render-offer [:offer/number offer-no])

(def offer-no-q '[:find ?o ?on
                  :in $ ?c
                  :where
                  [?c :customer/offers ?o]
                  [?o :offer/number ?on]])

(d/q offer-no-q @conn)

(d/q offer-no-q (d/as-of @conn as-of-date))

(d/q '[:find ?o ?on ?t
       :where
       [?o :offer/number ?on ?tx]
       [?tx :db/txInstant ?t]]
     (d/history @conn))

(d/q '[:find ?o ?on ?t
       :where
       [?o :offer/number ?on ?tx]
       [?tx :db/txInstant ?t]]
     (d/since @conn as-of-date))


(def effort-uri "datahike:file:///tmp/effort-store")

(comment

  (d/delete-database effort-uri)

  (d/create-database effort-uri :schema-on-read true :temporal-index false)

  )

(def tasks (d/q '[:find ?e ?n
                  :where
                  [?e :task/name ?n]] @conn))

(def effort-conn (d/connect effort-uri))

(d/transact effort-conn [{:effort/start-date (java.util.Date.)
                           :effort/task 30}
                          {:effort/start-date (java.util.Date.)
                           :effort/task 31}])

(d/q '[:find [(pull ?e [*]) ...]
       :where
       [?e :effort/task _]] @effort-conn)

(d/q '[:find [(pull ?e [*]) ...]
       :where
       [?e :effort/task _]] (d/as-of @effort-conn as-of-date))

(d/transact effort-conn [{:db/id 1 :effort/end-date (java.util.Date.)}])


(d/q '[:find ?tn ?sd
       :in $ $effort
       :where
       [$ ?t :task/name ?tn]
       [$effort ?e :effort/task ?t]
       [$effort ?e :effort/start-date ?sd]]
     @conn
     @effort-conn)

(->> (d/q '[:find ?tn ?sd ?ed
            :in $ $effort
            :where
            [$ ?t :task/name ?tn]
            [$effort ?e :effort/task ?t]
            [$effort ?e :effort/start-date ?sd]
            [$effort ?e :effort/end-date ?ed]
            ]
          @conn
          @effort-conn)
     (map (fn [[tn sd ed]]
            [tn (/ (- (.getTime ed) (.getTime sd)) 1000.0)])))

(d/transact conn [[:db/retractEntity [:offer/number "20190205-01"]]])

(d/q '[:find ?o ?on ?t
       :where
       [?o :offer/number ?on ?tx]
       [?tx :db/txInstant ?t]]
     @conn)

(d/q '[:find ?o ?on ?t
       :where
       [?o :offer/number ?on ?tx]
       [?tx :db/txInstant ?t]]
     (d/history @conn))

(d/transact conn [[:db.purge/entity [:offer/number "20190205-01"]]])

(d/q '[:find ?o ?on ?t
       :where
       [?o :offer/number ?on ?tx]
       [?tx :db/txInstant ?t]]
     @conn)

(d/q '[:find ?o ?on ?t
       :where
       [?o :offer/number ?on ?tx]
       [?tx :db/txInstant ?t]]
     (d/history @conn))
